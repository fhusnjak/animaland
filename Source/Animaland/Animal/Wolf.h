// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animal.h"
#include "Wolf.generated.h"

/**
 * 
 */
UCLASS()
class ANIMALAND_API AWolf : public AAnimal
{
	GENERATED_BODY()
protected:
	virtual FVector GetCurrentObjective() override;
};
