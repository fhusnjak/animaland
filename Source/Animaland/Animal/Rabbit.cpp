// Fill out your copyright notice in the Description page of Project Settings.


#include "Rabbit.h"

// Sets default values
ARabbit::ARabbit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARabbit::BeginPlay()
{
	Super::BeginPlay();

	m_timeToNewObjectiveCounter = -1;

}

// Called every frame
void ARabbit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	m_timeToNewObjectiveCounter -= DeltaTime;
	if (m_timeToNewObjectiveCounter < 0) {
		m_timeToNewObjectiveCounter = timeToNewObjective;

		m_currentObjective = GetNewObjective();
	}
}

FVector ARabbit::GetCurrentObjective() {
	return m_currentObjective;
}

FVector ARabbit::GetNewObjective()
{
	FVector location = GetActorLocation();

	FVector randomVector;
	randomVector.X = FMath::FRandRange(0 - objectiveMaxDistance, 0 + objectiveMaxDistance);
	randomVector.Y = FMath::FRandRange(0 - objectiveMaxDistance, 0 + objectiveMaxDistance);
	randomVector.Z = location.Z;

	return randomVector;
}