// Fill out your copyright notice in the Description page of Project Settings.


#include "Wolf.h"
#include "Rabbit.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

FVector AWolf::GetCurrentObjective()
{
	TSubclassOf<ARabbit> classToFind = ARabbit::StaticClass();
	TArray<AActor*> found;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classToFind, found);

	float minDistance = INFINITY;
	FVector location = GetActorLocation();
	for (AActor* a : found) {
		FVector loc = a->GetActorLocation();
		float distance = (loc - GetActorLocation()).Size();

		if (distance < minDistance) {
			location = loc;
			minDistance = distance;
		}
	}

	return location;

}
