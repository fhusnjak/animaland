// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Animal.generated.h"

UCLASS()
class ANIMALAND_API AAnimal : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAnimal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	FVector GetColissionAvoidanceForce();
	FVector GetGoalForce();
	FVector GetWallForce();
	FVector GetPersonalSpaceForce();
	FVector GetEvasionForce();
protected:
	virtual FVector GetCurrentObjective();
private:
	FVector m_currentVelocity;
public:
	UPROPERTY(EditAnywhere, Category = "PCA")
		float preferredSpeed;
	UPROPERTY(EditAnywhere, Category = "PCA")
		float preferredSpeedTau;

	UPROPERTY(EditAnywhere, Category = "PCA")
		float safeDistance;
	UPROPERTY(EditAnywhere, Category = "PCA")
		unsigned int steepness;
};
