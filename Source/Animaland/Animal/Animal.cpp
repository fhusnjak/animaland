// Fill out your copyright notice in the Description page of Project Settings.


#include "Animal.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

// Sets default values
AAnimal::AAnimal()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAnimal::BeginPlay()
{
	Super::BeginPlay();

	m_currentVelocity = FVector(0, 0, 0);
}

// Called every frame
void AAnimal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector force = GetColissionAvoidanceForce();

	m_currentVelocity += force * DeltaTime;
	FVector newLocation = GetActorLocation() + m_currentVelocity * DeltaTime;

	SetActorLocation(newLocation);
}



FVector AAnimal::GetColissionAvoidanceForce()
{
	FVector result = GetGoalForce() +
		GetWallForce() +
		GetPersonalSpaceForce() +
		GetEvasionForce();

	return result;
}

FVector AAnimal::GetGoalForce()
{
	FVector goalDir = GetCurrentObjective() - GetActorLocation();
	goalDir.Normalize();

	return (1.0 / preferredSpeedTau) * (goalDir * preferredSpeed - m_currentVelocity);
}

// TODO
FVector AAnimal::GetWallForce()
{
	FVector result(0, 0, 0);
	return result;
}

FVector AAnimal::GetPersonalSpaceForce() {
	TSubclassOf<AAnimal> classToFind = AAnimal::StaticClass();
	TArray<AActor*> found;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classToFind, found);

	const FVector& location = GetActorLocation();
	FVector result(0, 0, 0);
	for (AActor* a : found) {
		FVector distanceVector = a->GetActorLocation() - location;
		float distance = distanceVector.Size();

		distanceVector.Normalize();

		static const float epsilon = 0.0001;

		if (distance < epsilon)
			continue;

		if (safeDistance < distance)
			continue;

		result += distanceVector * (safeDistance - distance) / pow(distance, steepness);
	}

	return result;
}

// TODO
FVector AAnimal::GetEvasionForce() {
	FVector result(0, 0, 0);
	return result;
}

FVector AAnimal::GetCurrentObjective()
{
	return FVector();
}
