// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Animal.h"
#include "Rabbit.generated.h"

UCLASS()
class ANIMALAND_API ARabbit : public AAnimal
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARabbit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual FVector GetCurrentObjective() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	FVector GetNewObjective();
private:
	float m_timeToNewObjectiveCounter;
	FVector m_currentObjective;
public:
	UPROPERTY(EditAnywhere, Category="Objective")
		float timeToNewObjective;
	UPROPERTY(EditAnywhere, Category="Objective")
		float objectiveMaxDistance;
};
