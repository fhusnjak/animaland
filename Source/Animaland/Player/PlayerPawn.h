// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "../UI/UICanvas.h"
#include "../Animal/Rabbit.h"
#include "../Animal/Wolf.h"
#include "PlayerPawn.generated.h"

class UICanvas;

UCLASS()
class ANIMALAND_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
public:
	UFUNCTION()
		void OnSelectionChange(AnimalType newType);

	UFUNCTION()
		void OnTouch(ETouchIndex::Type touchIndex, FVector position);

	UPROPERTY(EditAnywhere, Category = "Animals BP")
		TSubclassOf<ACharacter> rabbitBP;

	UPROPERTY(EditAnywhere, Category = "Animals BP")
		TSubclassOf<ACharacter> wolfBP;

	UPROPERTY(EditAnywhere, Category = "Animals BP")
		TSubclassOf<ACharacter> deerBP;
private:
	AnimalType _selectedType = Wolf;

	bool listeningButtonChange = false;
};
