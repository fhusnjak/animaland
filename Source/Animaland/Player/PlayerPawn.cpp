// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "../UI/PlayerHUD.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!listeningButtonChange) {
		APlayerHUD* hud = Cast<APlayerHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());

		if (hud == nullptr)
			return;

		hud->canvas->OnSelectionChange.AddDynamic(this, &APlayerPawn::OnSelectionChange);

		listeningButtonChange = true;
	}
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &APlayerPawn::OnTouch);
}

void APlayerPawn::OnSelectionChange(AnimalType newType)
{
	_selectedType = newType;
}


void APlayerPawn::OnTouch(ETouchIndex::Type touchIndex, FVector position)
{
	FVector WorldLocation;
	FVector WorldDirection;

	if (!UGameplayStatics::GetPlayerController(this, 0)->DeprojectScreenPositionToWorld(position.X, position.Y, WorldLocation, WorldDirection))
		return;

	FHitResult Hit;
	FCollisionQueryParams TraceParams;
	FCollisionObjectQueryParams ObjectParams;

	static const float reach = 100000.0f;
	

	if (_selectedType == Remove) {
		GetWorld()->LineTraceSingleByObjectType(Hit, WorldLocation, WorldLocation + WorldDirection * reach, ECollisionChannel::ECC_Pawn);

		AActor* ActorHit = Hit.GetActor();
		if (ActorHit == nullptr)
			return;

		ActorHit->Destroy();
	}
	else {
		GetWorld()->LineTraceSingleByObjectType(Hit, WorldLocation, WorldLocation + WorldDirection * reach, ECollisionChannel::ECC_WorldStatic);

		AActor* ActorHit = Hit.GetActor();
		if (ActorHit == nullptr)
			return;

		FActorSpawnParameters spawnParameters;
		UClass* toSpawn;
		switch (_selectedType) {
		case Rabbit:
			toSpawn = rabbitBP.Get();
			break;
		case Deer:
			toSpawn = deerBP.Get();
			break;
		case Wolf:
			toSpawn = wolfBP.Get();
			break;
		default:
			return;
		}

		GetWorld()->SpawnActor(toSpawn, &Hit.Location, &FRotator::ZeroRotator, spawnParameters);
	}
}

