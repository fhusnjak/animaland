// Fill out your copyright notice in the Description page of Project Settings.


#include "UICanvas.h"
#include "AnimalButton.h"


void UUICanvas::NativeConstruct() {
	Super::NativeConstruct();

	BP_Wolf_Button->OnCheckStateChanged.AddDynamic(this, &UUICanvas::SelectWolf);
	BP_Deer_Button->OnCheckStateChanged.AddDynamic(this, &UUICanvas::SelectDeer);
	BP_Rabbit_Button->OnCheckStateChanged.AddDynamic(this, &UUICanvas::SelectRabbit);
	BP_Remove_Button->OnCheckStateChanged.AddDynamic(this, &UUICanvas::SelectRemove);

	SelectWolf(true);
}

void UUICanvas::SelectWolf(bool checked)
{
	ChangeState(Wolf);
}

void UUICanvas::SelectDeer(bool checked)
{
	ChangeState(Deer);

}

void UUICanvas::SelectRabbit(bool checked)
{
	ChangeState(Rabbit);
}

void UUICanvas::SelectRemove(bool checked)
{
	ChangeState(Remove);
}

void UUICanvas::ChangeState(AnimalType animalType) {
	BP_Wolf_Button->SetState(animalType == Wolf);
	BP_Deer_Button->SetState(animalType == Deer);
	BP_Rabbit_Button->SetState(animalType == Rabbit);
	BP_Remove_Button->SetState(animalType == Remove);

	OnSelectionChange.Broadcast(animalType);
}