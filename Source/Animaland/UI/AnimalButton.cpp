// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimalButton.h"
#include "Components/CheckBox.h"

void UAnimalButton::SetState(bool isChecked) {
	CheckBox->SetCheckedState(isChecked ? ECheckBoxState::Checked : ECheckBoxState::Unchecked);

}

void UAnimalButton::NativeConstruct()
{
	Super::NativeConstruct();

	CheckBox->OnCheckStateChanged.AddDynamic(this, &UAnimalButton::OnStateChange);
}


void UAnimalButton::OnStateChange(bool state)
{
	OnCheckStateChanged.Broadcast(state);

}
