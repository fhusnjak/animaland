// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "AnimalButton.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStateChanged, bool, bIsChecked);

class UCheckBox;

/**
 * 
 */
UCLASS()
class ANIMALAND_API UAnimalButton : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetState(bool isChecked);

	void NativeConstruct() override;
public:
	UPROPERTY(BlueprintAssignable, Category = "CheckBox|Event")
		FOnStateChanged OnCheckStateChanged;

public:
	UPROPERTY(meta = (BindWidget))
		UCheckBox* CheckBox;

	UFUNCTION()
		void OnStateChange(bool state);
};
