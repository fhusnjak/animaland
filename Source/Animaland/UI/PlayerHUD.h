// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"

#include "PlayerHUD.generated.h"

class UUICanvas;

/**
 * 
 */
UCLASS()
class ANIMALAND_API APlayerHUD : public AHUD
{
	GENERATED_BODY()
public:
	void BeginPlay() override;
public:
	UUICanvas* canvas;

	UPROPERTY(EditAnywhere, Category = "UI")
		TSubclassOf<UUserWidget> widgetClass;
};
