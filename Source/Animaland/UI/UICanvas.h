// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UICanvas.generated.h"

UENUM()
enum AnimalType {
	Deer, Wolf, Rabbit, Remove
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSelectionChange, AnimalType, newSelected);

class UAnimalButton;

/**
 * 
 */
UCLASS()
class ANIMALAND_API UUICanvas : public UUserWidget
{
	GENERATED_BODY()
public:
	void NativeConstruct() override;
public:
	UPROPERTY(BlueprintAssignable, Category = "CheckBox|Event")
		FOnSelectionChange OnSelectionChange;

	UPROPERTY(meta = (BindWidget))
		UAnimalButton* BP_Wolf_Button;

	UPROPERTY(meta = (BindWidget))
		UAnimalButton* BP_Deer_Button;

	UPROPERTY(meta = (BindWidget))
		UAnimalButton* BP_Rabbit_Button;

	UPROPERTY(meta = (BindWidget))
		UAnimalButton* BP_Remove_Button;

	UFUNCTION()
		void SelectWolf(bool checked);

	UFUNCTION()
		void SelectDeer(bool checked);

	UFUNCTION()
		void SelectRabbit(bool checked);

	UFUNCTION()
		void SelectRemove(bool checked);
private:
	void ChangeState(AnimalType animalType);
};
