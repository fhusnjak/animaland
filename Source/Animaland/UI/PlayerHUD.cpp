// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"
#include "Blueprint/UserWidget.h"
#include "UICanvas.h"

void APlayerHUD::BeginPlay() {
	Super::BeginPlay();

	canvas = CreateWidget<UUICanvas>(GetWorld(), widgetClass);

	if (canvas == nullptr)
		return;

	canvas->AddToViewport();
}